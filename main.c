/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: ksam <ksam@student.le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2020/01/18 19:39:52 by ksam         #+#   ##    ##    #+#       */
/*   Updated: 2020/01/18 19:49:04 by ksam        ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "get_next_line.h"
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int main(int ac, char **av)
{
    int fd, ret;
    char *line;

    fd = open(av[1], O_RDONLY);
    while (1)
    {
        ret = get_next_line(fd, &line);
        printf("[%d] [%d] '%s'\n", fd, ret, line);
        free(line);
        if (ret <= 0)
            break;
    }
    close(fd);
    return (0);
}