/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   get_next_line.h                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: ksam <ksam@student.le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2020/01/15 09:38:49 by ksam         #+#   ##    ##    #+#       */
/*   Updated: 2020/01/23 16:40:49 by ksam        ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# include <string.h>
# include <stdlib.h>
# include <unistd.h>

typedef struct		s_line
{
	char			*buffer_read;
	char			*buffer_temp;
	char			*buffer_stock;
	int				a_read;
	int				b_index;
}					t_line;

int					get_next_line(const int fd, char **line);
int					end_condition(t_line *list, char **line);
int					buffer_update(t_line *list, char **line);
int					ft_strichr(const char *s, int c);

size_t				ft_strlen(const char *s);
void				*ft_memset(void *b, int c, size_t len);
void				*ft_calloc(size_t count, size_t size);
char				*ft_strjoin(char *s1, char *s2);
char				*ft_substr(char *s, unsigned int start, size_t len);

#endif
